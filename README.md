# Scientific Programming for Engineers

![](http://phdcomics.com/comics/archive/phd053104s.gif)

## Practical information

- Every Thurday, Lectures from *10:15* to *12:00* and Exercises from *13:15* to *15:00*
- The room we will meet in is [GR A3 32](https://plan.epfl.ch/?room==GR%20A3%2032)
- Lectures will also be given through zoom and recorded [(https://epfl.zoom.us/j/61343133504)](https://epfl.zoom.us/j/61343133504)
- Videos published on a [SwitchTube](https://tube.switch.ch/channels/JIdScAdV3T)
- [Piazza forum](https://piazza.com/class/ktims16y1ee6ee)
- [GIT repository](https://gitlab.epfl.ch/anciaux/sp4e)
- [VirtualBox for the class](https://enacshare.epfl.ch/fwuNXfdzmy3U5RWZe4BaTgk7E9YjKFP)

## Summary

The students will acquire a solid knowledge on the processes necessary to design, write and use scientific software. Software design techniques will be used to program a multi-usage particles code, aiming at providing the link between algorithmic/complexity, optimization and program designs.
Content

- Object Oriented Paradigm
- C/C++ and Python programming (class, operator, template, design patterns, STL)
- Programming techniques, code factorization
- Pointers, memory management, data structures
- Linear system solving (Eigen library)
- C++/Python coupling (pybind)
- Post-treatment: Paraview, numpy/scipy, matplotlib


## Classical problems

- series calculations
- solar system and many-body calculation
- sparse linear algebra.
