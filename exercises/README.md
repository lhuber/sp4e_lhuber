Setup environment
``` sh
conda env create -f environment.yml
```

Activate environment
``` sh
conda activate sp4e
```

