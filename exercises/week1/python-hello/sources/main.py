#!/usr/bin/env python
import sys
import copy
import re
import argparse

from series import computeSeries

def get_clean_input():
    argv = copy.deepcopy(sys.argv)
    print("argv init", argv) 
    if re.search('/.+/ipython', argv[0]):
        del argv[0]

    for default in ['--simple-prompt', '-i']:
        if default in argv:
            argv.remove(default)

    return argv

def run_exercise_2(arg0):
    # argv = get_clean_input()
    # if not len(argv):
        # raise Exception("Wrong number of input arguments.")

    print(f"Hello {arg0}")

def run_exercise_3(arg0):
    # argv = get_clean_input()
    # if not len(argv):
        # raise Exception("Wrong number of input arguments.")
    # n_iterations = int(argv[0])
    n_sum = computeSeries(arg0)

    print(f"Hello {n_sum}")
    
    
def main():
    print("Hello World!")
    print(len(sys.argv))


if (__name__) == "__main__":
    parser = argparse.ArgumentParser(description='Print input and series..')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                        help='an integer for the calculation')
    args = parser.parse_args()
    
    # breakpoint()
    # main()

    run_exercise_2(args.integers[0])
    run_exercise_3(args.integers[0])
