""" Compute series of iterations. """

def computeSeries(nIterations):
    return sum([kk for kk in range(1, nIterations+1)])
    
