/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <cmath>
#include <iostream>
/* -------------------------------------------------------------------------- */

double tanh(double x){
  return (exp(x) - exp(-x))/(exp(x) + exp(-x));
}

double tanh_fact(double x){
  return (1 - exp(-x)/exp(x))/(1 + exp(-x)/exp(x));
}

double tanh_minus_1(double x){
  // Two avoid overflow, subtract before
  return -2*exp(-x) / (exp(x) + exp(-x));
}

double evolution(double pn, double v, double dt){
  return pn + dt*v;
}

void exo1(){
  std::cout << "Exercise 1" << std::endl;
	;
  int a1 = 0.5;
  std::cout << a1 << std::endl;

  double a2 = 1/2;
  std::cout << a2 << std::endl;

  // For int division problem:
  // python2 -> yes
  // Python3 -> specifi int-division is clled differently

  // For variables:
  // python dynamically (implicetly) creates variable types
  // BUT it can be explicitely specified, and it that case it could happen too
}
/* -------------------------------------------------------------------------- */
void exo2(){
  std::cout << "Exercise 2" << std::endl;
  std::cout.precision(17);
  
  double x = 10.;
  double a = exp(x);
  std::cout << a << std::endl;

  std::cout << std::endl;
  std::cout <<  "tanh(x) computation \n";
  int n_vals = 10;
  double tanh_vals [n_vals];
  double tanh_fact_vals [n_vals];
  
  double value_input;
  for (int ii = 0; ii < n_vals; ii++){
	value_input = ii*10;
	tanh_vals[ii] = tanh(value_input);
	tanh_fact_vals[ii] = tanh_fact(value_input);

	std::cout << "x=" << value_input;
	std::cout <<" tanh=" << tanh_vals[ii];
	std::cout <<" tanh_fact=" << tanh_fact_vals[ii];
	std::cout << std::endl;
  }

  // doul
  std::cout << std::endl;
  std::cout <<  "tanh(x) -1 computation \n";
  for (int ii = 0; ii < n_vals; ii++){
	value_input = ii*10;
	tanh_vals[ii] = tanh(value_input) - 1;
	tanh_fact_vals[ii] = tanh_minus_1(value_input);

	std::cout << "x=" << value_input;
	std::cout <<" tanh=" << tanh_vals[ii];
	std::cout <<" tanh_fact=" << tanh_fact_vals[ii];
	std::cout << std::endl;
  }

}
/* -------------------------------------------------------------------------- */

void exo3(){

  int n_iterations = 40;
  double velocity = 3 * pow(10, 8);
  
  double p0 = 0;
  double p1 = pow(10, -3);

  double dt = 1000;
  for (int ii = 0; ii < n_iterations; ii++){
	p0 = evolution(p0, velocity, dt);
	p1 = evolution(p1, velocity, dt);

	std::cout << "it=" << ii;
	std::cout << " p0=" << p0;
	std::cout << " p1=" << p1;
	std::cout << " delta_dist=" << p1-p0;
	// std::cout << std::endl;
	std::cout << std::endl;

	// Rounding error from the beginning (1 -> 0.0009765)
	// Starting from it=29 -> complete overflow
	// with p0=9.e12 p1=9e12+1e-3 -> digit error ~9e-15
	// I would have expected it to happen only one iteration though,
	// due to general precision of 17 digits.
  }
}
/* -------------------------------------------------------------------------- */


int main(int argc, char ** argv){
  // exo1();
  // exo2();
  exo3();

  return EXIT_SUCCESS;
}
