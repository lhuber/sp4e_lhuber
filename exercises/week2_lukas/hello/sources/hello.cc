#include <string>
#include <iostream>

#include "series.hh"
/* -------------------------------------------------------------------------- */

int char_to_int(char* input){
  /* Returns integer from a char-pointer (/string) */
  return *input - 48;
}

int main(int argc, char ** argv){
  if (argc <= 1) {
	std::cout << "Wrong number of arguments input.\n";
	return 1;
  }
  // write your code here
  std::cout << "Hello " << argv[1] << "\n";

  // Character to integer
  int n_iterations = char_to_int(argv[1]);
  int sum_n = computeSeries(n_iterations);
  
  std::cout << "S_n = " << sum_n << "\n";
  
  return EXIT_SUCCESS;
}
