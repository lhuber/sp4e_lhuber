/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
/* -------------------------------------------------------------------------- */


void pi_calc(int it_max){
  float pi_calc = 0;
  float float_k;
  for (int kk = 1; kk < (it_max+1); kk++){
	float_k = kk;
	// pi_calc += 1.0/(kk*kk);
	pi_calc += 1.0/(float_k*float_k);
  }
  // pi_calc = pow(6*pi_calc, 0.5);
  pi_calc = std::sqrt(6*pi_calc);

  std::cout << "Iterative pi calculation:" << std::endl;
  std::cout << "it_max=" << it_max << std::endl;
  std::cout << "pi=" << pi_calc << std::endl;
  std::cout << std::endl;
}


void pi_calc_inv(int it_max){
  float pi_calc = 0;
  float float_k;
  for (int kk = it_max; kk > 0; kk--){
	float_k = kk;
	pi_calc += 1.0/(float_k*float_k);
  }
  pi_calc = pow(6*pi_calc, 0.5);

  std::cout << "Inverse-iterative pi calculation:" << std::endl;
  std::cout << "it_max=" << it_max << std::endl;
  std::cout << "pi=" << pi_calc << std::endl;
  std::cout << std::endl;
}


int main(int argc, char ** argv){
  // your code here
  std::cout.precision(8);
  
  pi_calc(pow(10, 4));
  pi_calc(pow(10, 5));
  pi_calc(pow(10, 6));
  pi_calc(pow(10, 7));
  pi_calc(pow(10, 8));

  pi_calc_inv(pow(10, 4));
  pi_calc_inv(pow(10, 5));
  pi_calc_inv(pow(10, 6));
  pi_calc_inv(pow(10, 7));
  pi_calc_inv(pow(10, 8));

  // While the sum of many small number will add up to a non-neglegible number.
  // Starting off with the smaller ones will consider them,
  // while starting off with the large number, the small summands will end up
  // as a rounding error.
  
  return EXIT_SUCCESS;
}

