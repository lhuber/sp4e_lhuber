#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <map>
#include <set>
#include <numeric>
#include <typeinfo>
/* -------------------------------------------------------------------------- */


void cout_vector(std::vector<double> vector){
  std::cout << "vector: " << std::endl;
  std::cout << "[";
  for (auto& vv: vector) {
    std::cout <<  vv << ", ";
    // use first and second
  }
  std::cout << "]" << std::endl;
}


void write_coordinates_to_file(void){
  std::ofstream myfile;
  myfile.open("toto.txt");
  myfile << "(1.5, 1.0, 2.0)\n";
  myfile.close();
};

void check_input_string(char ** argv){
  std::string myName = "Lukash";
    
  std::string argument = std::string(argv[1]);

  if (argument == myName){
    std::cout << "You have a nice name" << std::endl;
  }
}

void read_coordinates_from_file(){
  std::string line;
  std::ifstream myfile ("toto.txt");

  double x, y, z;
  int n_dimension = 3;
  double arr[n_dimension];
  
  if (myfile.is_open())
  {
    // Assumption of one line only
    std::getline (myfile, line);
    
    // Remove first and last character
    line = line.substr(1, line.size() - 2);

    std::string::size_type n = 0;
    while ( ( n = line.find( ',', n ) ) != std::string::npos)
    {
      line.replace( n, 1, 1, ' ' );
      n++;
    }
    std::istringstream is( line );
    is >> x >> y >> z;

    std::istringstream iss( line );

    for(int ii = 0; ii < n_dimension; ii++) {
      iss >> arr[ii];
    }

    // std::stringstream stringer(line);
    // stringer >> x >> y >> z;

  } else {
    std::cout << "Unable to open file";
  }

  std::cout << "Got my variables." << std::endl;
  std::cout << x << ", " << y << ", " << z << std::endl;
  std::cout << "Array:";
  for(int ii = 0; ii < n_dimension; ii++) {
    std::cout << arr[ii] << ", ";
  }
  std::cout << std::endl;
}


std::vector<double> get_pi_vector(){
  int n_values = 100;

  std::vector<double> pi_vect;

  for(int ii = 0; ii < n_values; ii++) {
    pi_vect.push_back(2*M_PI * ii / n_values);
  }

  if (false) {
    std::cout << "Vector:";
    for(int ii = 0; ii < n_values; ii++) {
      std::cout << pi_vect[ii] << ", ";
    }
    std::cout << std::endl;
  }

  std::cout << "Vector size: " << pi_vect.size() << std::endl;

  // Resize
  int n_new_vals = 120;
  pi_vect.resize(n_new_vals);
  
  std::cout << "New size: " << pi_vect.size() << std::endl;
  std::cout << "Fille value: " << pi_vect[pi_vect.size() - 1] << std::endl;

  // Make normal size again
  pi_vect.resize(n_values); 

  std::vector< std::pair<double, double> > sin_vect;

  for (int ii = 0; ii < n_values; ii++) {
    // std::cout << "pi value" << pi_vect[ii];
    sin_vect.push_back( std::make_pair ( pi_vect[ii], std::sin ( pi_vect[ii] ) ) );
  }
  
  // Print to file
  std::ofstream myfile;
  myfile.open("sinus_file.txt");
  myfile << "Value, Sinus\n";
  for (int ii = 0; ii < n_values; ii++) {
    myfile << sin_vect[ii].first;
    myfile << ", "; 
    myfile << sin_vect[ii].second;
    myfile << std::endl;
  }
  myfile.close();

  return pi_vect;
}


void try_map_and_set(void){
  using Point = std::array<double, 3>;
  
  std::map<std::string, Point> map;

  Point origin_coordinates = {1.0, 0.0, 0.0};
  
  map["sun"] = origin_coordinates;

  auto it = map.find("sun");
  // std::array<int, 3> value_array {1, 2, 3};
  
  // std::cout << "Sun value: " << std::endl;

  if (it == map.end())
    map["sun"] = {1.0, 0.0, 0.0};
     
  // std::cout << *it;
  // for (const auto& str : value_array) {
    // std::cout << str << std::endl;
  // }
  // std::cout << std::endl;
  // std::cout << typeid(value_array).name() << std::endl;

  std::map<std::string, Point> solarSystem;
  solarSystem["mercury"] = {0.25, 0., 0.};
  solarSystem["earth"] = {1.0, 0., 0.};
  solarSystem["jupiter"] = {5.0, 0., 0.};
  solarSystem["sun"] = {0, 0, 0};

  std::set<std::string> solarSystemSet;
  std::set<std::string> mapSet;

  std::map<std::string, Point>::iterator it_map;
  for (it_map = solarSystem.begin(); it_map != solarSystem.end(); it_map++) {
    solarSystemSet.insert(it_map->first);
  }

  for (it_map = map.begin(); it_map != map.end(); it_map++) {
    mapSet.insert(it_map->first);
  }

  std::vector<std::string> vv(10);
  
  auto ll = std::set_intersection(
    mapSet.begin(), mapSet.end(),  solarSystemSet.begin(), solarSystemSet.end(), vv.begin());
                             
  vv.resize(ll - vv.begin());

  std::map<std::string, Point> combinedMap;
  
  for(std::vector<std::string>::iterator it_name = vv.begin(); it_name != vv.end(); it_name++) {
    combinedMap[*it_name] = map[*it_name];
  }

  // Print combined set
  std::cout << "Combined set:" << std::endl;
  for (it_map = combinedMap.begin(); it_map != combinedMap.end(); it_map++) {
    std::cout << it_map->first << ": " ;
    for(int ii = 0; ii < 3; ii++)
      std::cout << it_map->second[ii] << ", " ;
    std::cout << std::endl;
  }
}


void standard_algorithms(void){
  int nn = 10;
  std::vector<double> vec_n (nn);

  // Use std::fill to input constant values
  std::fill(vec_n.begin(), vec_n.end(), -1);

  // for (const auto& str : value_array) {
  // std::cout << str << std::endl;
  // }

  std::cout << "Vector elements: " << std::endl;
  std::cout << "[";
  for (auto& v_val: vec_n) {
    std::cout <<  v_val << ", ";
    // use first and second
  }
  std::cout << "]" << std::endl;

  // Use std::iota to input incremental values
  std::iota(vec_n.begin(), vec_n.end(), 1);
  cout_vector(vec_n);

  // Lambda function to square a double
  auto doubleSquarer = [](double& dd) { dd = dd*dd; };
  
  double var_double = 3.0;

  std::cout << "Old double: " << var_double << std::endl;
  doubleSquarer(var_double);
  std::cout << "New double: " << var_double << std::endl;

  // for (auto& v_val: vec_n) {
    // doubleSquarer(v_val);
  // }

  std::for_each(vec_n.begin(), vec_n.end(), doubleSquarer);

  std::cout << "Vector after squaring application.";
  cout_vector(vec_n);

  double vectorSum;
  auto vectorSummer = [&vectorSum](double summand) {vectorSum += summand; };
  std::for_each(vec_n.begin(), vec_n.end(), vectorSummer);

  std::cout << "Vector sum: " << vectorSum << std::endl;

  double analyticResult = (1.0)*nn*(nn+1)*(2*nn+1)/6;
  std::cout << "Analytic result: " << analyticResult << std::endl;

  
  std::cout << "With std::accumulate: " ;
  std::cout << std::accumulate(vec_n.begin(), vec_n.end(), 0) << std::endl;
}

void pi_estimate_comparison(void) {
  int n_tot = 1000;
  std::vector<double> vectorNatural (n_tot);
  
  std::iota(vectorNatural.begin(), vectorNatural.end(), 1);
  std::reverse(vectorNatural.begin(), vectorNatural.end());
  std::for_each(vectorNatural.begin(), vectorNatural.end(), [] (double& nn) { nn = 1.0/(nn*nn); });
  double piEstimate = std::accumulate(vectorNatural.begin(), vectorNatural.end(), 0.0);
  
  piEstimate = pow(6*piEstimate, 0.5);
  std::cout << "Pi no-loop: " << piEstimate << std::endl;

  // Looper
  float pi_calc = 0;
  float float_k;
  for (int kk = n_tot; kk > 0; kk--){
	float_k = kk;
	pi_calc += 1.0/(float_k*float_k);
  }
  pi_calc = pow(6*pi_calc, 0.5);

  std::cout << "Pi loop: " << pi_calc << std::endl;
}


int main(int argc, char ** argv){
  // Compare if someone randomly mentioned their name..
  
  // check_input_string(argv);
  
  // write_coordinates_to_file();

  // read_coordinates_from_file();

  // std::vector<double> pi_vector =  get_pi_vector();

  // try_map_and_set();

  // standard_algorithms();
  
  pi_estimate_comparison();

  return EXIT_SUCCESS;
}



