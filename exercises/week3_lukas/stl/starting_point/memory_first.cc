#include <vector>
#include <array>
#include <memory>
#include <iostream>


void values_stack_old(){
  std::cout << "Array creation on stack.\n";
  int values[10];

  for (int& v : values)
    v = 0;

  for (int i = 0; i < 10; ++i)
    std::cout << values[i] << " ";
    
  std::cout << std::endl;
}

void values_stack_modern(){
  std::cout << "Array (modern) creation on stack.\n";
  std::array<int, 10> values;

  for (int& v : values)
    v = 0;

  for (int i = 0; i < 10; ++i)
    std::cout << values[i] << " ";
    
  std::cout << std::endl;
}


void values_heap_old(){
  std::cout << "Array creation on heap.\n";
  int n_values = 10;
  int *values = new int[n_values];

  for (int ii = 0; ii < n_values; ii++)
    values[ii] = 0;
  
  for (int i = 0; i < 10; ++i)
    std::cout << values[i] << " ";

  delete [] values;
    
  std::cout << std::endl;
}

void values_heap_modern(){
  std::cout << "Array (modern) creation on heap.\n";
  int n_values = 10;
  std::vector<int> values(n_values);

  for (int ii = 0; ii < n_values; ii++)
    values[ii] = 0;
  
  for (int i = 0; i < 10; ++i)
    std::cout << values[i] << " ";

  std::cout << std::endl;
}



int main() {
  values_stack_old();
  values_stack_modern();
  
  values_heap_old();
  values_heap_modern();
    
  return 0;
}
