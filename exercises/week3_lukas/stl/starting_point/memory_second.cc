#include <vector>
#include <array>
#include <memory>
#include <iostream>

std::vector<int>* stack_allocation(int n) {
  std::vector<int> vector;
  vector.resize(n);
  std::vector<int> * vec_ptr = &vector;

  return vec_ptr;
}

// std::vector<int>* stack_allocation_modern(int n) {
  // How to?
  // std::vector<int> vector;
  // vector.resize(n);
  // std::vector<int> * vec_ptr = &vector;

  // return vec_ptr;
// }


std::vector<int>* heap_allocation(int n) {
  std::vector<int> * vec_ptr = new std::vector<int>;

  vec_ptr->resize(n);
  return vec_ptr;
}

std::unique_ptr<std::vector<int> > heap_allocation_modern(int n) {
  std::unique_ptr<std::vector<int>> vec_ptr = std::make_unique<std::vector<int> >();
  
  // std::raise(SIGINT);
  std::cout << "Doing\n";
  
  vec_ptr->resize(n);
  std::cout << "Done\n";
  return vec_ptr;
}


int main() {
  std::vector<int> * stack_values = stack_allocation(10);
  std::vector<int> * heap_values = heap_allocation(10);

  std::cout << stack_values->size() << ", " << heap_values->size() << std::endl;
  // delete heap_values;

  std::cout << "Starting Novel.\n";
  // stack_allocation_modern(10);
  std::unique_ptr<std::vector<int> > heap_modern= heap_allocation_modern(10);

  std::cout << heap_modern->size() << std::endl;
  
  return 0;
}
