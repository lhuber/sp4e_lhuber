#include <vector>
#include <array>
#include <memory>
#include <iostream>


int main() {
  auto unique = std::make_unique<std::vector<int>>(10);

  std::unique_ptr<std::vector<int>> other = nullptr;
  
  // Transfer ownership of unique pointer
  other = std::move(unique);
}
