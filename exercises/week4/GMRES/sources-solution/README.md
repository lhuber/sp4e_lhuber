# Instructions to launch the program

- In order to launch the program, simply launch:

```python
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2
```

With the coefficients $a_ij$ and $b_i$ the terms of the matrix $A$ and vector $b$ in the quadratic form $1/2 x^T A x - x^T b$, as advertised by the help:

```python
python3 ./main.py -h
```

This will compute the minimum of the quadratic function using one optimizer.

- In order to change the method you can add an additional argument:

```python
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -m BFGS
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -m GMRES
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -m my_GMRES
```

- In order to make a plot, use the following option:

```python
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -p 1
```
Example of launch:

```python
python3 ./main.py -m my_GMRES 8 1 1 3 2 4 -p 1
```