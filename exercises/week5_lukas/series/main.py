""" Main function."""
from series import ComputeArithmetic, ComputePi

if (__name__) == "__main__":
    if True:
        my_class = ComputeArithmetic()
    else:
        my_class = ComputePi()

    print("I computed: ",  my_class.compute(N=1e6))

