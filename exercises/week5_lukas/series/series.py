""" """
import numpy as np

from abc import ABC, abstractmethod

class Series:
    def __init__(self):
        self.current_term = 0
        self.current_value = 0

    @abstractmethod
    def compute(self, N: int):
        pass

    def getAnalyticPrediction(self):
        raise Exception("Pure virtual function.")

class ComputeArithmetic(Series):
    def compute(self, N: int) -> float:
        series_value = np.sum(np.arange(1, N+1))
        return series_value


class ComputePi(Series):
    def compute(self, N: int) -> float:
        return np.sqrt(6*np.sum(1/np.arange(1, N+1)**2))

class DumperSeries(ABC):
    def __init__(self, series):
        self.series = series

    @abstractmethod
    def dump(self):
        pass

class PrintSeries(DumperSeries):
    def __init__(self, frequency: int, maxiter: int):
        self.frequency = frequency
        self.maxiter = maxiter
        
    def dump(self, maxiter):
        pass
