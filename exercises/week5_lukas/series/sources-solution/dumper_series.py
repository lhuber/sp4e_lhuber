import sys
from abc import ABC, abstractmethod


class DumperSeries(ABC):

    def __init__(self, series):
        self.series = series
        self.precision = 4
        self._file = sys.stdout
        self.outfile = ''

    def __del__(self):
        pass

    @abstractmethod
    def dump(self):
        pass

    def setPrecision(self, precision):
        self.precision = precision

    def setOutFile(self, outfile):
        self.outfile = outfile
        if outfile == '':
            return

        self._file = open(outfile, 'w')

    def __str__(self):
        return self.dump()
